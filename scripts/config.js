const path = require('path')
const babel = {
  presets: ['@babel/preset-env'],
  plugins: [
    ['@babel/plugin-transform-react-jsx']
  ],
  compact: false
}
const webpack = {
  entry: ['babel-polyfill', './src/main.js'],
  mode: 'production',
  module: {
    rules: [
      {
        use: [
          {
            loader: 'babel-loader',
            options: babel
          }
        ]
      }
    ]
  },
  output: {
    path: path.resolve('./index/'),
    filename: 'script.js'
  },
  // Unfortunately, preact cannot be used with the latest version of react-redux
  // resolve: {
  //   alias: {
  //     'react': 'preact-compat',
  //     'react-dom': 'preact-compat'
  //   }
  // },
  devtool: 'source-map'
}
module.exports = {
  port: 8080,
  babel,
  webpack
}
