const webpack = require('webpack')
const fs = require('fs')
const path = require('path')

const buildConfig = require('./config.js')
const generatePage = require('./generate-page.js')

webpack(buildConfig.webpack, () => {
  fs.writeFileSync(
    './index/index.html',
    generatePage()
  )
  ;[
    'style.css',
    'favicon.png',
    'favicon.ico'
  ].forEach(file =>
    fs.createReadStream(`./src/${file}`).pipe(fs.createWriteStream(`./index/${file}`))
  )

  // copies static files
  const copyFilesInFolder = (source, destination) => {
    const destinationFull = path.join(destination, path.basename(source))
    if (!fs.existsSync(destinationFull)) {
      fs.mkdirSync(destinationFull)
    }
    fs.readdirSync(source).filter(f => !f.startsWith('.')).forEach(file => {
      const filePath = path.join(source, file)
      if (fs.lstatSync(filePath).isDirectory()) {
        copyFilesInFolder(filePath, destinationFull)
      } else {
        fs.copyFileSync(filePath, `${destinationFull}/${file}`)
      }
    })
  }
  if (fs.existsSync('./src/static')) {
    copyFilesInFolder(path.resolve('./src/static'), path.resolve('./index'))
  }
})
