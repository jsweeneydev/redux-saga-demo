import { call, takeEvery, takeLatest, put, select } from 'redux-saga/effects'

// Note: NOT the redux-saga delay
export const delay = delay => new Promise((resolve, reject) => setTimeout(resolve, delay))

export const pretendFetch = (delay, willSucceed) => new Promise((resolve, reject) => {
  setTimeout(() => {
    if (!willSucceed) reject(Error('Arbitrary failue'))
    else resolve()
  }, delay)
})

export function * useEvery () {
  yield call(delay, 1000)
  yield put({ type: 'USEEVERY_INCREMENT_COMPLETE' })
}

export function * useLatest () {
  yield call(delay, 1000)
  yield put({ type: 'USELATEST_INCREMENT_COMPLETE' })
}

export function * internalHandleProcess (index, willSucceed) {
  try {
    yield call(pretendFetch, 3000, willSucceed)
    yield put({ type: 'ERRORHANDLING_PROCESS_COMPLETE', payload: { index } })
  } catch (error) {
    yield put({ type: 'ERRORHANDLING_PROCESS_FAILED', payload: { index } })
  }
}

export function * handleProcess () {
  const state = yield select()
  const errorHandlingState = state.errorHandling
  const index = errorHandlingState.processes.length - 1
  const willSucceed = !errorHandlingState.nextWillFail

  yield internalHandleProcess(index, willSucceed)
}

function * saga () {
  yield takeEvery('USEEVERY_INCREMENT_REQUESTED', useEvery)
  yield takeLatest('USELATEST_INCREMENT_REQUESTED', useLatest)
  yield takeEvery('ERRORHANDLING_PROCESS_REQUESTED', handleProcess)
}

export default saga
