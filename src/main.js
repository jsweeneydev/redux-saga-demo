import React from 'react'
import { render } from 'react-dom'
import { Provider, connect } from 'react-redux'
import { createStore, compose, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import saga from './sagas.js'

import mainReducer from './mainReducer.js'

// const defaultState = require('./default.js').state
import Main from './main.jsx'

// const splice = (array, start, deleteCount, ...items) => {
//   const newArray = array.concat()
//   newArray.splice(start, deleteCount, ...items)
//   return newArray
// }

const sagaMiddleware = createSagaMiddleware()
const store = createStore(
  mainReducer,
  compose(
    applyMiddleware(sagaMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : state => state
  )
)
sagaMiddleware.run(saga)

const ConnectedMain = connect(
  // Map state to props
  state => state,
  // Map dispatch to props
  dispatch => ({
    useEveryDispatch: {
      onIncrementRequested: () => dispatch({
        type: 'USEEVERY_INCREMENT_REQUESTED'
      })
    },
    useLatestDispatch: {
      onIncrementRequested: () => dispatch({
        type: 'USELATEST_INCREMENT_REQUESTED'
      })
    },
    errorHandlingDispatch: {
      onProcessRequested: ({ willFail }) => dispatch({
        type: 'ERRORHANDLING_PROCESS_REQUESTED',
        payload: { willFail }
      })
    }
  })
)(Main)

render(
  <Provider store={store}>
    <ConnectedMain />
  </Provider>,
  document.querySelector('.app-wrapper')
)
