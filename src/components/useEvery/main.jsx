import React from 'react'

class UseEvery extends React.Component {
  render () {
    const {
      counter,
      processing,
      onIncrementRequested
    } = this.props

    const spinner = processing ? <span>Hang on...</span> : null

    return <div className='experiment'>
      <h3>UseEvery</h3>
      <article>
        <div className='sample'>
          <span>{counter}</span><br />
          <button
            onClick={onIncrementRequested}
          >Increment</button>
          {spinner}
        </div>
        <div className='description'>
          <pre>useEvery</pre> will accept every button activation and spin up a saga. Try spam-clicking the button.
        </div>
      </article>
    </div>
  }
}

export default UseEvery
