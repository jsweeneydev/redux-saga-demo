export default (
  state = {
    counter: 0,
    processing: false
  },
  action = { type: '', payload: {} }
) => {
  const { type } = action
  switch (type) {
    case 'USEEVERY_INCREMENT_REQUESTED': {
      return {
        ...state,
        processing: true
      }
    }
    case 'USEEVERY_INCREMENT_COMPLETE': {
      return {
        ...state,
        counter: state.counter + 1,
        processing: false
      }
    }
  }
  return state
}
