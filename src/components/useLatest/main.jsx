import React from 'react'

class UseLatest extends React.Component {
  render () {
    const {
      counter,
      processing,
      onIncrementRequested
    } = this.props

    const spinner = processing ? <span>Hang on...</span> : null

    return <div className='experiment'>
      <h3>UseLatest</h3>
      <article>
        <div className='sample'>
          <span>{counter}</span><br />
          <button
            onClick={onIncrementRequested}
          >Increment</button>
          {spinner}
        </div>
        <div className='description'>
          <pre>useLatest</pre> will only register the last button activation. If you spam-click the button, nothing will increment until you stop and a delay passes.
        </div>
      </article>
    </div>
  }
}

export default UseLatest
