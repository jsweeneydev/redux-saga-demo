const splice = (array, start, deleteCount, ...items) => {
  const newArray = array.concat()
  newArray.splice(start, deleteCount, ...items)
  return newArray
}

export default (
  state = {
    processes: [
      // {
      //   processing: true,
      //   success: false
      // }
    ],
    nextWillFail: false
  },
  action = { type: '', payload: {} }
) => {
  const { type, payload } = action
  switch (type) {
    case 'ERRORHANDLING_PROCESS_REQUESTED': {
      const { willFail } = payload
      return {
        ...state,
        processes: state.processes.concat({
          processing: true,
          success: false
        }),
        nextWillFail: willFail
      }
    }
    case 'ERRORHANDLING_PROCESS_COMPLETE': {
      const { index } = payload
      return {
        ...state,
        processes: splice(state.processes, index, 1, {
          ...state.processes[index],
          processing: false,
          success: true
        })
      }
    }
    case 'ERRORHANDLING_PROCESS_FAILED': {
      const { index } = payload
      return {
        ...state,
        processes: splice(state.processes, index, 1, {
          ...state.processes[index],
          processing: false,
          success: false
        })
      }
    }
  }
  return state
}
