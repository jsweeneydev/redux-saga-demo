import React from 'react'

class UseLatest extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      willFail: false
    }
    this.toggleWillFail = this.toggleWillFail.bind(this)
    this.handleProcess = this.handleProcess.bind(this)
  }
  toggleWillFail (event) {
    this.setState({ willFail: event.target.checked })
  }
  handleProcess () {
    this.props.onProcessRequested({ willFail: this.state.willFail })
  }
  render () {
    const {
      processes
    } = this.props

    return <div className='experiment'>
      <h3>Error Handling</h3>
      <article>
        <div className='sample'>
          <span style={{ fontSize: '2em' }}>
            {(() => processes.map((process, index) =>
              <span key={index} style={{ color:
                process.processing
                  ? 'gray'
                  : process.success
                    ? 'blue'
                    : 'red'
              }}>■</span>
            ))()}
          </span><br />
          <label>
            <input
              type='checkbox'
              checked={this.state.willFail}
              onChange={this.toggleWillFail}
            />
            Break it
          </label>
          <button
            onClick={this.handleProcess}
          >Process Thing</button>
        </div>
        <div className='description'>
          Clicking the "process" button will generate a square.
          Depending on the state of the checkbox, the square will either "succeed" or "fail".
          If the saga succeeds, the square will turn blue after three seconds. Otherwise, it will turn red.
        </div>
      </article>
    </div>
  }
}

export default UseLatest
