import { combineReducers } from 'redux'
import useEveryReducer from './components/useEvery/reducer.js'
import useLatestReducer from './components/useLatest/reducer.js'
import errorHandlingReducer from './components/errorHandling/reducer.js'

export default combineReducers({
  useEvery: useEveryReducer,
  useLatest: useLatestReducer,
  errorHandling: errorHandlingReducer
})
