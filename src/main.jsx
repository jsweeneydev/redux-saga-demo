import React from 'react'

import UseEvery from './components/useEvery/main.jsx'
import UseLatest from './components/useLatest/main.jsx'
import ErrorHandling from './components/errorHandling/main.jsx'

// import test from './test.md'
// console.log(test)

class Main extends React.Component {
  render () {
    const {
      useEvery,
      useEveryDispatch,
      useLatest,
      useLatestDispatch,
      errorHandling,
      errorHandlingDispatch
    } = this.props

    return <div>
      <h1>
        <img src='./favicon.png' alt='redux-saga logo' width='64' height='64' />
        Redux-Saga Experiments
      </h1>
      <article>
        <div className='description'>
          Just a collection of some of my <a href='https://redux-saga.js.org/'>redux-saga</a> experiements.<br />
          The source code can be found here: <a href='https://bitbucket.org/jsweeneydev/redux-saga-demo/src'>https://bitbucket.org/jsweeneydev/redux-saga-demo/src</a>
        </div>
      </article>
      <UseEvery
        {...useEvery}
        {...useEveryDispatch}
      />
      <UseLatest
        {...useLatest}
        {...useLatestDispatch}
      />
      <ErrorHandling
        {...errorHandling}
        {...errorHandlingDispatch}
      />
    </div>
  }
}
export default Main
