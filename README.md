Just a handful of [redux-saga](https://redux-saga.js.org/) use cases that I created while studying it.



Setup:

```sh
npm install
```

To run in development mode:

```sh
node .
```

To build for the web:

```sh
npm run build
```

