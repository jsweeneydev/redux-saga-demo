import test from 'tape'

import { call, select } from 'redux-saga/effects'

import {
  delay,
  pretendFetch,
  useEvery,
  useLatest,
  internalHandleProcess
} from '../src/sagas.js'

test('UseEvery Saga Test', assert => {
  const generator = useEvery()
  assert.deepEqual(
    generator.next().value,
    call(delay, 1000)
  )
  assert.end()
})

test('UseLatest Saga Test', assert => {
  const generator = useLatest()
  assert.deepEqual(
    generator.next().value,
    call(delay, 1000)
  )
  assert.end()
})

test('HandleProcess Saga Test', assert => {
  const generator = internalHandleProcess(0, true)
  assert.deepEqual(
    generator.next().value,
    call(pretendFetch, 3000, true)
  )
  assert.end()
})
