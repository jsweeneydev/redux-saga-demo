require('babel-polyfill')
const babelConfig = require('../scripts/config.js').babel
require('@babel/register')(babelConfig)
require('./tests.js')
